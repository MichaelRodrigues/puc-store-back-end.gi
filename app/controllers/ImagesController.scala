package controllers

import play.mvc.Controller
import play.mvc.Result
import play.mvc.Results._
import java.io.File

/**
  * Created by js on 6/1/17.
  */
class ImagesController extends Controller {
  def getImage(image: String): Result = {
    val img = image.replace("%20", " ")

    val file = new File("/home/aplic/prodimages/products/" + img)
    println(file.getAbsolutePath)
    ok(file)
  }
}
