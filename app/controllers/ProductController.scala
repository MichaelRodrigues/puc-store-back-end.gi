package controllers

import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import dao.ProductDAO
import io.swagger.annotations.Api
import javax.inject.Inject
import play.api.Configuration
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.{JsError, Json}
import service.UserService
import utils.auth.DefaultEnv
import utils.responses.rest.Bad
import play.api.mvc.{AbstractController, ControllerComponents}
import play.modules.reactivemongo._
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

@Api(value = "Registration")
class ProductController @Inject()(components: ControllerComponents,
                                  userService: UserService,
                                  configuration: Configuration,
                                  silhouette: Silhouette[DefaultEnv],
                                  credentialsProvider: CredentialsProvider,
                                  authInfoRepository: AuthInfoRepository,
                                  val reactiveMongoApi: ReactiveMongoApi,
                                  messagesApi: MessagesApi)
                                 (implicit ex: ExecutionContext) extends AbstractController(components) with I18nSupport {

  val productDAO = new ProductDAO(reactiveMongoApi)

  def newProduct = silhouette.SecuredAction.async(parse.json) { implicit request =>
    request.body.validate[models.security.Product].map { product =>
      val operation = userService.retrieve(request.authenticator.loginInfo).map({
        case Some(user) =>
          val id = Some(java.util.UUID.randomUUID.toString.replaceAll("-",""))

          productDAO.add(product.copy(user = user.email, enabled = false, id = id))
          Future.successful(Ok(Json.obj("id" -> id.get)))
        case None => Future.successful(BadRequest(Json.toJson(Bad(message = "User was not found 1."))))
      }) recover {
        case _ => Future.successful(BadRequest(Json.toJson(Bad(message = "User was not found 2."))))
      }

      operation.flatten

    }.recoverTotal {
      error => Future.successful(BadRequest(Json.toJson(Bad(message = JsError.toJson(error)))))
    }
  }

  def getByUser = silhouette.SecuredAction.async { implicit request =>
    userService.retrieve(request.authenticator.loginInfo).map({
      case Some(user) =>
        productDAO.findByUser(user.email).map(products => {
          Future.successful(Ok(Json.obj("result" -> products)))
        }) recover {
          case _ => Future.successful(NotFound(Json.toJson(Bad(message = "The products were not found."))))
        } flatten
      case None => Future.successful(BadRequest(Json.toJson(Bad(message = "User was not found."))))
    }) recover {
      case _ => Future.successful(BadRequest(Json.toJson(Bad(message = "User was not found."))))
    } flatten
  }


  def getAll() = Action {
    val allProducts = productDAO.findAll().map(products => {
      Ok(Json.obj("result" -> products))
    }) recover {
      case _ => NotFound(Json.toJson(Bad(message = "The products were not found.")))
    }
    Await.result(allProducts, 3.seconds)
  }

  def search(value: String) = Action {
    val products = productDAO.searchProduct(value).map(products => {
      Ok(Json.obj("result" -> products))
    }) recover {
      case _ => NotFound(Json.toJson(Bad(message = "The products were not found.")))
    }
    Await.result(products, 3.seconds)
  }

  def remove(productID: String)  = silhouette.SecuredAction.async { implicit request =>
    productDAO.remove(productID)
    Future.successful(Ok(Json.obj("id" -> productID)))
  }

}
