package controllers

import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import dao.{ProductDAO, SaleDAO}
import io.swagger.annotations.Api
import javax.inject.Inject
import models.security.Sale
import play.api.Configuration
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.{JsError, Json}
import play.api.mvc.{AbstractController, ControllerComponents}
import play.modules.reactivemongo._
import service.UserService
import utils.auth.DefaultEnv
import utils.responses.rest.Bad

import scala.concurrent.{ExecutionContext, Future}

@Api(value = "Sale")
class SaleController @Inject()(components: ControllerComponents,
                               userService: UserService,
                               configuration: Configuration,
                               silhouette: Silhouette[DefaultEnv],
                               credentialsProvider: CredentialsProvider,
                               authInfoRepository: AuthInfoRepository,
                               val reactiveMongoApi: ReactiveMongoApi,
                               messagesApi: MessagesApi)
                              (implicit ex: ExecutionContext) extends AbstractController(components) with I18nSupport {

  val saleDAO = new SaleDAO(reactiveMongoApi)
  val productDAO = new ProductDAO(reactiveMongoApi)

  def newSale = silhouette.SecuredAction.async(parse.json) { implicit request =>
    request.body.validate[models.security.SaleInput].map { saleInput =>
      val operation = userService.retrieve(request.authenticator.loginInfo).map({
        case Some(user) =>
          val id    = java.util.UUID.randomUUID.toString.replaceAll("-","")
          val sale  = Sale(id, user, System.currentTimeMillis(), saleInput.products, false, false)

          saleDAO.add(sale)
          Future.successful(Ok(Json.toJson(sale)))
        case None => Future.successful(BadRequest(Json.toJson(Bad(message = "User was not found 1."))))
      }) recover {
        case _ => Future.successful(BadRequest(Json.toJson(Bad(message = "User was not found 2."))))
      }

      operation.flatten

    }.recoverTotal {
      error => Future.successful(BadRequest(Json.toJson(Bad(message = JsError.toJson(error)))))
    }
  }

  def getShoppingByUser = silhouette.SecuredAction.async { implicit request =>
    userService.retrieve(request.authenticator.loginInfo).map({
      case Some(user) =>
        saleDAO.findShoppingByUser(user.email).map(sales => {
          Future.successful(Ok(Json.obj("result" -> sales)))
        }) recover {
          case _ => Future.successful(NotFound(Json.toJson(Bad(message = "The sales were not found."))))
        } flatten
      case None => Future.successful(BadRequest(Json.toJson(Bad(message = "Sales were not found."))))
    }) recover {
      case _ => Future.successful(BadRequest(Json.toJson(Bad(message = "Sales were not found."))))
    } flatten
  }

  def getSaleByUser = silhouette.SecuredAction.async { implicit request =>
    userService.retrieve(request.authenticator.loginInfo).map({
      case Some(user) =>
        productDAO.findSaleByUser(user.email).map(products => {
          Future.successful(Ok(Json.obj("result" -> products)))
        }) recover {
          case _ => Future.successful(NotFound(Json.toJson(Bad(message = "The sales were not found."))))
        } flatten
      case None => Future.successful(BadRequest(Json.toJson(Bad(message = "Sales were not found."))))
    }) recover {
      case _ => Future.successful(BadRequest(Json.toJson(Bad(message = "Sales were not found."))))
    } flatten
  }

  def getAll = silhouette.SecuredAction.async { implicit request =>
    userService.retrieve(request.authenticator.loginInfo).map({
      case Some(user) =>
        saleDAO.findSalesAll().map(sales => {
          Future.successful(Ok(Json.obj("result" -> sales)))
        }) recover {
          case _ => Future.successful(NotFound(Json.toJson(Bad(message = "The sales were not found."))))
        } flatten
      case None => Future.successful(BadRequest(Json.toJson(Bad(message = "Sales were not found."))))
    }) recover {
      case _ => Future.successful(BadRequest(Json.toJson(Bad(message = "Sales were not found."))))
    } flatten
  }

}
