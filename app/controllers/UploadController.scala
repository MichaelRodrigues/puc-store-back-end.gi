package controllers

import java.nio.file.Paths

import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.util.Clock
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import io.swagger.annotations.{Api, ApiOperation}
import javax.inject.Inject
import play.api.Configuration
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import service.UserService
import utils.auth.DefaultEnv
import java.util.UUID.randomUUID

import scala.concurrent.{ExecutionContext, Future}

@Api(value = "Upload")
class UploadController @Inject()(components: ControllerComponents,
                                 userService: UserService,
                                 configuration: Configuration,
                                 silhouette: Silhouette[DefaultEnv],
                                 clock: Clock,
                                 credentialsProvider: CredentialsProvider,
                                 messagesApi: MessagesApi)
                                (implicit ex: ExecutionContext) extends AbstractController(components) with I18nSupport {


  @ApiOperation(value = "Upload photos")
  def productImage = silhouette.SecuredAction.async { implicit request =>
    val picture       = request.body.asMultipartFormData.get.file("file").get
    val extension     = picture.contentType.get.split("/").last
    val newFilename   = randomUUID().toString.replaceAll("-","") + "." + extension

    picture.ref.moveTo(Paths.get(s"/home/aplic/prodimages/products/$newFilename"), replace = true)

    Future.successful(Ok(Json.obj("picture" -> newFilename)))
  }

  
}
