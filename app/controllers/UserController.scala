package controllers

import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.api.util.Clock
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import dao.UserDAO
import io.swagger.annotations.{Api, ApiOperation}
import javax.inject.Inject
import play.api.Configuration
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}
import play.modules.reactivemongo.ReactiveMongoApi
import service.UserService
import utils.auth.DefaultEnv
import utils.responses.rest.Bad
import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

@Api(value = "User")
class UserController @Inject()(components: ControllerComponents,
                               userService: UserService,
                               configuration: Configuration,
                               silhouette: Silhouette[DefaultEnv],
                               clock: Clock,
                               credentialsProvider: CredentialsProvider,
                               val reactiveMongoApi: ReactiveMongoApi,
                               messagesApi: MessagesApi)
                              (implicit ex: ExecutionContext) extends AbstractController(components) with I18nSupport {


  val userDAO = new UserDAO(reactiveMongoApi)

  @ApiOperation(value = "Change User Activated to Active")
  def enableUser(userEmail: String) = silhouette.SecuredAction.async { implicit request =>
    userService.retrieve(request.authenticator.loginInfo).map({
      case Some(_) =>
        Try(userDAO.updateActivatedStatus(activated = true, userEmail)) match {
          case Success(_) => Future.successful(Ok(Json.obj("user" -> userEmail)))
          case Failure(_) =>  Future.successful(NotFound(Json.toJson(Bad(message = "The user were not found."))))
        }
      case None => Future.successful(BadRequest(Json.toJson(Bad(message = "User was not found."))))
    }) recover {
      case _ => Future.successful(BadRequest(Json.toJson(Bad(message = "User was not found."))))
    } flatten
  }

  @ApiOperation(value = "Change User Activated to Disable")
  def disableUser(userEmail: String) = silhouette.SecuredAction.async { implicit request =>
    userService.retrieve(request.authenticator.loginInfo).map({
      case Some(_) =>
        Try(userDAO.updateActivatedStatus(activated = false, userEmail)) match {
          case Success(_) => Future.successful(Ok(Json.obj("user" -> userEmail)))
          case Failure(_) =>  Future.successful(NotFound(Json.toJson(Bad(message = "The user were not found."))))
        }
      case None => Future.successful(BadRequest(Json.toJson(Bad(message = "User was not found."))))
    }) recover {
      case _ => Future.successful(BadRequest(Json.toJson(Bad(message = "User was not found."))))
    } flatten
  }

  def getAll() = Action {
    val allUsers = userDAO.findAll().map(users => {
      Ok(Json.obj("result" -> users))
    }) recover {
      case error =>  NotFound(Json.toJson(Bad(message = "The users were not found. " + error.getMessage)))
    }
    Await.result(allUsers, 3.seconds)
  }

}
