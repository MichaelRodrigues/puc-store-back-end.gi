package dao


import com.sksamuel.elastic4s.analyzers.WhitespaceAnalyzer
import com.sksamuel.elastic4s.{Preference, RefreshPolicy}
import play.api.libs.json.{JsObject, Json}
import reactivemongo.play.json._
import reactivemongo.play.json.collection._
import reactivemongo.api.Cursor
import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.http.{ElasticClient, ElasticProperties, HttpClient}
import com.typesafe.config.ConfigFactory
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.bson.BSONDocument

import scala.concurrent.{Await, ExecutionContext, Future}

/**
  * An implementation of the auth info DAO which stores the data in database.
  */
class ProductDAO(val reactiveMongoApi: ReactiveMongoApi)(implicit ex: ExecutionContext) {

  def products = reactiveMongoApi.database.map(_.collection[JSONCollection]("product"))
  def sales = reactiveMongoApi.database.map(_.collection[JSONCollection]("sales"))

  implicit lazy val format = Json.format[models.security.Product]

  val client = ElasticClient(ElasticProperties(ConfigFactory.load().getString("elastic.uri")))

  def add(product: models.security.Product): Future[models.security.Product] = {
    val id             = java.util.UUID.randomUUID.toString.replaceAll("-","")
    val productBuilder = Json.toJson(product.copy(id = Some(id))).as[JsObject]

    client.execute {
      indexInto("products" / "product").fields("name" -> product.name,
        "product_id" -> id,
        "enabled" -> true) id id
    }.await


    products.flatMap(_.insert(productBuilder)).flatMap {
      _ =>  Future.successful(product)
    }
  }

  def remove(productID: String): Unit = {
    println(productID)
    client.execute {delete(id = productID) from "products" / "product"}.await
    products.map( db => db.remove(Json.obj("id" -> productID)))
  }


  def findByUser(user: String): Future[List[models.security.Product]] = {
    products.map( db => {
      db.find(Json.obj("user" -> user)).cursor[models.security.Product]().
        collect[List](25, // get up to 25 documents
        Cursor.FailOnError[List[models.security.Product]]())
    }).flatten
  }

  def findAll(): Future[List[models.security.Product]] = {
    products.map( db => {
      db.find(Json.obj()).cursor[models.security.Product]().
        collect[List](25, // get up to 25 documents
        Cursor.FailOnError[List[models.security.Product]]())
    }).flatten
  }

  def findSaleByUser(user: String): Future[List[models.security.Product]] = {
    val foundSalesFuture = sales.map( db => {
      db.find(BSONDocument("products.user" -> user)).cursor[models.security.Sale]().
        collect[List](25, // get up to 25 documents
        Cursor.FailOnError[List[models.security.Sale]]())
    }).flatten

    foundSalesFuture.map(foundSale => {
      foundSale.flatMap(fs => {
        fs.products.filter(product => product.user.equals(user))
      })
    })
  }

  def searchProduct(value: String): Future[List[models.security.Product]] = {
    val searchResponse = client.execute {
      search("*") types("products", "product") query {
        wildcardQuery("name", value)
      }
      //stringQuery("**") allowLeadingWildcard true analyzeWildcard true analyzer WhitespaceAnalyzer autoGeneratePhraseQueries true defaultField "name" boost 6.5 enablePositionIncrements true fuzzyMaxExpansions 4 fuzzyPrefixLength 3 lenient true phraseSlop 10 tieBreaker 0.5 operator "OR" rewrite "writer"

    }.await

    val query = Json.obj("id" -> Json.obj("$in" -> searchResponse.result.ids))

    products.map( db => {
      db.find(query).cursor[models.security.Product]().
        collect[List](25, // get up to 25 documents
        Cursor.FailOnError[List[models.security.Product]]())
    }).flatten
  }
}
