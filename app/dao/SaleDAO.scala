package dao

import play.api.libs.json.{JsObject, Json}
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.Cursor
import reactivemongo.play.json._
import reactivemongo.play.json.collection._

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the auth info DAO which stores the data in database.
  */
class SaleDAO(val reactiveMongoApi: ReactiveMongoApi)(implicit ex: ExecutionContext) {

  def sales = reactiveMongoApi.database.map(_.collection[JSONCollection]("sales"))
  def products = reactiveMongoApi.database.map(_.collection[JSONCollection]("products"))

  implicit lazy val format = Json.format[models.security.Sale]

  def add(sale: models.security.Sale): Future[models.security.Sale] = {
    val productBuilder = Json.toJson(sale).as[JsObject]
    sales.flatMap(_.insert(productBuilder)).flatMap {
      _ =>  Future.successful(sale)
    }
  }

  def findShoppingByUser(user: String): Future[List[models.security.Sale]] = {
    sales.map( db => {
      db.find(Json.obj("user.email" -> user)).cursor[models.security.Sale]().
        collect[List](25, // get up to 25 documents
        Cursor.FailOnError[List[models.security.Sale]]())
    }).flatten
  }

  def findSalesAll(): Future[List[models.security.Sale]] = {
    sales.map( db => {
      db.find(Json.obj()).cursor[models.security.Sale]().
        collect[List](25, // get up to 25 documents
        Cursor.FailOnError[List[models.security.Sale]]())
    }).flatten
  }

}
