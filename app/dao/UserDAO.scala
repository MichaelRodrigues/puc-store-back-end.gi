package dao

import play.api.libs.json.{JsObject, Json}
import play.modules.reactivemongo.ReactiveMongoApi
import reactivemongo.api.Cursor
import reactivemongo.bson.BSONDocument
import reactivemongo.play.json._
import reactivemongo.play.json.collection._

import scala.concurrent.{ExecutionContext, Future}

/**
  * An implementation of the auth info DAO which stores the data in database.
  */
class UserDAO(val reactiveMongoApi: ReactiveMongoApi)(implicit ex: ExecutionContext) {

  def users = reactiveMongoApi.database.map(_.collection[JSONCollection]("user"))

  implicit lazy val format = Json.format[models.security.User]

  def updateActivatedStatus(activated: Boolean, user: String): Unit = {
    val selector = BSONDocument("email" -> user)
    val modifier = BSONDocument("$set" -> BSONDocument("activated" -> activated))

    users.flatMap( db => db.update(selector, modifier))
  }

  def findAll(): Future[List[models.security.User]] = {
    users.map( db => {
      db.find(Json.obj()).cursor[models.security.User]().
        collect[List](25, // get up to 25 documents
        Cursor.FailOnError[List[models.security.User]]())
    }).flatten
  }

}
