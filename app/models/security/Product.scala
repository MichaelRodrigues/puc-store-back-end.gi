package models.security

import play.api.libs.json.{Json, _}

import scala.util.{Failure, Success, Try}

case class Product(id: Option[String], name: String, price: Double, description: String, departament: String, pictures: Array[String], enabled: Boolean, user: String)

object Product {

  implicit val reader = Json.reads[Product]
  implicit val writer = Json.writes[Product]

  implicit object ProductWrites extends OWrites[Product] {
    def writes(product: Product): JsObject =
      product.id match {
        case Some(id) =>
          Json.obj(
            "id" -> id,
            "name" -> product.name,
            "price" -> product.price,
            "description" -> product.name,
            "departament" -> product.departament,
            "pictures" -> product.pictures,
            "enabled" -> product.enabled,
            "user" -> product.user
          )
        case _ =>
          Json.obj(
            "name" -> product.name,
            "price" -> product.price,
            "description" -> product.name,
            "departament" -> product.departament,
            "pictures" -> product.pictures,
            "enabled" -> product.enabled,
            "user" -> product.user
          )
      }

    implicit object ProductReads extends Reads[Product] {
      def reads(json: JsValue): JsResult[Product] = json match {
        case product: JsObject =>
          Try {
            val id = (product \ "_id" \ "$oid").asOpt[String]
            val name = (product \ "name").as[String]
            val price = (product \ "price").as[Double]
            val description = (product \ "description").as[String]
            val departament = (product \ "departament").as[String]
            val pictures = (product \ "pictures").as[Array[String]]
            val enabled = (product \ "enabled").as[Boolean]
            val user = (product \ "user").as[String]

            JsSuccess(
              new Product(
                id,
                name,
                price,
                description,
                departament,
                pictures,
                enabled,
                user)
            )
          } match {
            case Success(value) => value
            case Failure(cause) => JsError(cause.getMessage)
          }
        case _ => JsError("expected.jsobject")
      }
    }

  }

}
