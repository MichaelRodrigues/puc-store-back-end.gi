package models.security

import play.api.libs.json.{Json, _}
import scala.util.{Failure, Success, Try}

case class Sale(id: String, user: User, moment: Long, products: List[Product], paid: Boolean, delivered: Boolean)

object Sale {

  implicit val reader = Json.reads[Sale]
  implicit val writer = Json.writes[Sale]

  implicit object SaleWrites extends OWrites[Sale] {
    def writes(sale: Sale): JsObject =
      Json.obj(
        "id" -> sale.id,
        "user" -> sale.user,
        "moment" -> sale.moment,
        "products" -> sale.products,
        "delivered" -> sale.delivered,
        "paid" -> sale.paid
      )

    implicit object SaleReads extends Reads[Sale] {
      def reads(json: JsValue): JsResult[Sale] = json match {
        case sale: JsObject =>
          Try {
            val id = (sale \ "_id" \ "$oid").as[String]
            val user = (sale \ "user").as[User]
            val moment = (sale \ "moment").as[Long]
            val products = (sale \ "products").as[List[Product]]
            val delivered = (sale \ "delivered").as[Boolean]
            val paid = (sale \ "paid").as[Boolean]

            JsSuccess(
              new Sale(
                id,
                user,
                moment,
                products,
                delivered,
                paid)
            )
          } match {
            case Success(value) => value
            case Failure(cause) => JsError(cause.getMessage)
          }
        case _ => JsError("expected.jsobject")
      }
    }

  }

}
