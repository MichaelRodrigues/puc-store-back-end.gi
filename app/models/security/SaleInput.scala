package models.security

import play.api.libs.json.{Json, _}

import scala.util.{Failure, Success, Try}

case class SaleInput(products: List[Product])

object SaleInput {

  implicit val reader = Json.reads[SaleInput]
  implicit val writer = Json.writes[SaleInput]

  implicit object SaleInputWrites extends OWrites[SaleInput] {
    def writes(saleInput: SaleInput): JsObject =
      Json.obj(
        "products" -> saleInput.products
      )

    implicit object SaleInputReads extends Reads[SaleInput] {
      def reads(json: JsValue): JsResult[SaleInput] = json match {
        case saleInput: JsObject =>
          Try {
            val products = (saleInput \ "products").as[List[Product]]
            JsSuccess(new SaleInput(products))
          } match {
            case Success(value) => value
            case Failure(cause) => JsError(cause.getMessage)
          }
        case _ => JsError("expected.jsobject")
      }
    }

  }

}
