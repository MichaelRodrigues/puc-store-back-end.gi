package models.security

import io.swagger.annotations.{ApiModel, ApiModelProperty}

@ApiModel(description = "SignUp object")
case class SignUp(
  @ApiModelProperty(value = "seu email", required = true, example = "bruce.wayne@test.com") identifier: String,
  @ApiModelProperty(required = true, example = "this!Password!Is!Very!Very!Strong!") password: String,
  @ApiModelProperty(value = "e-mail address", required = true, example = "brune.wayne@test.com") email: String,
  @ApiModelProperty(value = "seu nome", required = true, example = "James") name: String,
  @ApiModelProperty(value = "seu profile", required = true, example = "Comprar") profile: String)